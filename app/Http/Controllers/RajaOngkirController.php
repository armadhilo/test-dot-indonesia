<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use App\Http\Requests\GetCityRequest;
use App\Http\Requests\RajaOngkirRequest;
use Illuminate\Http\Request;

class RajaOngkirController extends Controller
{

    private function endpointRajaOngkir($url, $method, $request){

        $curl = curl_init();
        $raja_ongkir_key = config('setting.raja-ongkir.key');

        if($method == 'GET'){
            curl_setopt_array($curl, array(
                CURLOPT_URL => "$url?id=$request->city_id&province=$request->province_id",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key: $raja_ongkir_key"
                ),
            ));
        }elseif($method == 'POST'){
            curl_setopt_array($curl, array(
                CURLOPT_URL => "$url",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "origin=$request->origin&destination=$request->destination&weight=$request->weight&courier=$request->courier",
                CURLOPT_HTTPHEADER => array(
                    "content-type: application/x-www-form-urlencoded",
                    "key: $raja_ongkir_key"
                ),
            ));
        }

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, true);

    }

    public function checkOngkir(RajaOngkirRequest $request){

        $response = $this->endpointRajaOngkir('https://api.rajaongkir.com/starter/cost', 'POST', $request);

        $data['origin'] = $response['rajaongkir']['origin_details']['city_name'];
        $data['destination'] = $response['rajaongkir']['destination_details']['city_name'];
        $data['weight'] = $request->weight;
        $data['result'] = $response['rajaongkir']['results'];

        $message = 'Cost return successfully';
        return ApiFormatter::success(200, $message, $data);
    }

    public function getCity(GetCityRequest $request){

        $response = $this->endpointRajaOngkir('https://api.rajaongkir.com/starter/city', 'GET', $request);

        $message = 'Get City return successfully';
        return ApiFormatter::success(200, $message, $response);
    }
}
